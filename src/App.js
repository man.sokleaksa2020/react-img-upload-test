/* resources
  - https://developer.mozilla.org/en-US/docs/Web/API/FileReader
  - https://www.youtube.com/watch?v=o2nmgbZaGMw&t=581s
*/
/* by bad genius */
import React, {Component} from 'react'
import './App.css'


export class App extends Component {
  state = { 
    /* image palce holder */
    profileImg : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
}
  imageHandler = (e) => {
    const reader = new FileReader ();
    /* This event is triggered each time the reading operation is successfully completed. */
    reader.onload = () => {
      /* readyState property provides the current state of the reading operation a FileReader is in */
      if(reader.readyState ===2  ) {
        /* result property returns the file's contents */
        this.setState({profileImg: reader.result})
      }
    }
    /* readAsDataURL method is used to read the contents of the specified File */
    reader.readAsDataURL(e.target.files[0])
  }
    render () {
      const{ profileImg} = this.setState;
      return (
          <div className= "page">
              <div className=  "container">
                  <h1 className="heading">Add your Image</h1>
                  <div className="img-holder">
                    <img src= {profileImg}  alt= "" id= "img" className= "img"/>
                  </div>
                  <input type= "file" accept="image/*" name= "image-upload" id= "input" onChange={this.imageHandler}/>
                  <div className = "label">
                    <label className="image-upload" htmlFor = "input">
                      <i>add_photo</i>
                      Choose your Photo
                    </label>
                  </div>

              </div>

          </div>
      );
    }
}
export default App;

